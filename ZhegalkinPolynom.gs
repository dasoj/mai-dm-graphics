function gega(xyz,f) {
  if (!xyz) xyz=SpreadsheetApp.getActiveSheet().getRange('A3:A10').getValues();
  if (!f) f=SpreadsheetApp.getActiveSheet().getRange('B3:B10').getValues();
  var ge=[];
  let L='+';
  if (f[7][0]) ge[0]='1'; 
  for (var j=1;j<=7;j++){
    for (var i=0;i<=(7-j);i++){
     var fa=f[i][j]=(f[i][j-1]+f[i+1][j-1])%2;
      }
    if (fa) { ge.push(xyz[i-1][0]);
              if (xyz[i-1][0].length>1) L='-';
    } 
  
    for (var i=7-j+1;i<=7;i++){
     f[i][j]='';
      }
  }

  SpreadsheetApp.getActiveSheet().getRange('B3:I10').setValues(f);
  SpreadsheetApp.getActiveSheet().getRange('D2').setValue(ge.reverse().join('+'));
  return L;
}

function zanovo(){
  var f=[]
  for (var j=0;j<=7;j++){ f[j]=[];f[j][0]='0';}
  SpreadsheetApp.getActiveSheet().getRange('B3:B10').setValues(f);
}

function replaceF(str) {
//  str='(y⊃z)⊃¬(x∼y)';  //'(((x&y)∨(¬x&¬y))∨z)&x';
  str = calcImpl(str);
//  str=str.replace(/1/g,'(x∨¬x)');
//  str=str.replace(/\+¬/g,'∼');
//  str=str.replace(/\+/g,'∼¬');
  str = calcPlus(str);
  str = calcEqviv(str);
  var str1=str;
  str=str.replace(/¬/g,'!');
  str=str.replace(/∨/g,'||');
  str='Number('+str.replace(/&/g,'&&')+')';
  let func = new Function('x', 'y','z','return ' + str);
 // let f=func(0,0,1);
 //     f=func(1,1);

  return {f:func,str:str1};
}

function calcTablTrue(str) { // вариант, основанный на парсинге (новый logica.gs)
  //str='¬(x∨y∨z)'; 
  SpreadsheetApp.getActive().getRange('P2').setValue(str);
  var arr=tablTrue(3);
  var sdnf=tablTrueD(3);
  var sknf=tablTrueK(3);
  var rez=[];
  arr.forEach(([x,y,z],i,a)=>{let str1=str.replace(/x/g,x).replace(/y/g,y).replace(/z/g,z);
                              let r=logica7.parse(str1); 
                              a[i].push(r); 
                              rez[i]=[r];
                              if(r)sknf[i]=[''];
                              else sdnf[i]=[''];});
  let T0='-';T1='-';L='-';S='-';M='-';
  if (rez[0][0]=='1') T1='+'; 
  if (rez[7][0]=='0') T0='+'; 
  
  SpreadsheetApp.getActive().getRange('K3:N10').setValues(arr);
  SpreadsheetApp.getActive().getRange('P3:P10').setValues(sdnf);
  SpreadsheetApp.getActive().getRange('R3:R10').setValues(sknf);
  
  let dnf=sdnf.filter(([x])=>x!='');
  let knf=sknf.filter(([x])=>x!='');
  SpreadsheetApp.getActive().getRange('P13').setValue('('+dnf.join(')∨(')+')');
  SpreadsheetApp.getActive().getRange('P14').setValue('('+knf.join(')&(')+')');
  L=gega(false,rez);
  S=samod(rez);
  M=monot(arr);
  let tlsm=[[T0,T1,L,S,M]];
  SpreadsheetApp.getActive().getRange('C14:G14').setValues(tlsm);
  return true;                          
}

function calcTabl(str) { // вариант, основанный на раскрытии скобок (старый)
//  str='¬y';
  SpreadsheetApp.getActive().getRange('P2').setValue(str);
  var {f,str}=replaceF(str);
  SpreadsheetApp.getActive().getRange('R2').setValue(str);
  var arr=tablTrue(3);
  var sdnf=tablTrueD(3);
  var sknf=tablTrueK(3);
  var rez=[];
  arr.forEach(([x,y,z],i,a)=>{let r=f(x,y,z); 
                              a[i].push(r); 
                              rez[i]=[r];
                              if(r)sknf[i]=[''];
                              else sdnf[i]=[''];});
 // Logger.log(arr);
  let T0='-';T1='-';L='-';S='-';M='-';
  if (rez[0][0]=='1') T1='+'; 
  if (rez[7][0]=='0') T0='+'; 
  
  SpreadsheetApp.getActive().getRange('K3:N10').setValues(arr);
  SpreadsheetApp.getActive().getRange('P3:P10').setValues(sdnf);
  SpreadsheetApp.getActive().getRange('R3:R10').setValues(sknf);
  
  let dnf=sdnf.filter(([x])=>x!='');
  let knf=sknf.filter(([x])=>x!='');
  SpreadsheetApp.getActive().getRange('P13').setValue('('+dnf.join(')∨(')+')');
  SpreadsheetApp.getActive().getRange('P14').setValue('('+knf.join(')&(')+')');
  L=gega(false,rez);
  S=samod(rez);
  M=monot(arr);
  let tlsm=[[T0,T1,L,S,M]];
  SpreadsheetApp.getActive().getRange('C14:G14').setValues(tlsm);
  return true;
}

function samod(rez){
  let n=rez.length/2;
  let S='+';
  for (let i=0; i<=n;i++){
    if(rez[i]==rez[n-i]) return '-';
    
  }
  return S;
}

function monot(arr){ // arr -- таблица функции
 let n=arr[0].length;
 let N=arr.length;
  let M='+';
  for (let i=0; i<N-1;i++){
    let x=arr[i].slice(0,-1);
    for (let j=i+1; j<N;j++){
     if(x.pareto(arr[j],n-1)&&arr[i][n-1]<arr[j][n-1]) return '-';
    }
  }
  return M; 
}

function calcEqviv(str){   // Избавляемся от '∼': A∼B=(¬A∨B)&(A∨¬B)
//    var vvv='((¬(x∨y)&(((¬x&¬y)∨(x&y))))∨((x∨y)&¬(((¬x&¬y)∨(x&y)))))'
    var reg2 = /^(.+?)(∼)(.+?)$/i; // ищем '∼'
//    str='(y⊃z)⊃¬(x∼y)';
    var str1=str;
    var arr=[];
    var res;  
     while ((res= reg2.exec(str1)) !== null){
       var A=lDoSk(res[1]);
       var B=rDoSk(res[3]);
       var C=Eqviv2(A,B);
       str1=str1.replace(A+'∼'+B,C);
       arr.push(str1);
     }
    
   var result = 1; 
 return str1//arr;  
}

function calcPlus(str){   // Избавляемся от '+': A+B=(A∨B)&(¬A∨¬B)
    var reg2 = /^(.+?)(\+)(.+?)$/i; // ищем '+'
    //str='¬(x∨y)+(¬x∼¬y)';
    var str1=str;
    var arr=[];
    var res;  
     while ((res= reg2.exec(str1)) !== null){
       var A=lDoSk(res[1]);
       var B=rDoSk(res[3]);
       var C=Pluse2(A,B);
       str1=str1.replace(A+'+'+B,C);
       arr.push(str1);
     }
    
   var result = 1; 
 return str1//arr;  
}


function Pluse2(A,B){   // реализуем '+': A+B=(A∨B)&(¬A∨¬B)
//A='(¬x∨t)';
//B='¬y';
return '('+A+'∨'+B +')'+'&'+'('+ne(A)+'∨'+ne(B)+')';
}

function Eqviv2(A,B){   // реализуем '∼': A∼B=(¬A∨B)&(A∨¬B)
//A='(¬x∨t)';
//B='¬y';
return '('+ne(A)+'∨'+B +')'+'&'+'('+A+'∨'+ne(B)+')';
}

function calcImpl(str){   // Избавляемся от '⊃': A⊃B=¬A∨B
 //   var str=document.getElementById("formula0").value;
 //   var rez=document.getElementById("result");
 // str='¬(((x∨y)⊃(¬y⊃¬x))⊃¬z)∨(x⊃t)'; 
    var arr=[];
    var str1=str;
    var reg=/(¬?[a-zA-Z]|[^\)])⊃([^\(])/i;    // знак ⊃: до нет ) [переменная или её отрицание] после нет ( => переменные 
    var reg1 = /(¬?\(.+\))([&,⊃,∨,])(¬?\(.+\))/i; // между () и () знак операции
    var reg2 = /(¬?[a-z]|¬?\(.+\))(⊃)(¬?[a-z]|¬?\(.+\))/i; // между () и () или между двумя переменными с отр или без  знак операции
    var res;  
     while ((res= reg2.exec(str1)) !== null){
       var r1=res[1];
       r1=lDoSkOtr(r1);
       r1=r1+'∨'+res[3];
       str1=str1.replace(res[0],r1);
       arr.push(str1);
     }
//   var result = "(((x∨y)⊃(¬y⊃¬x))∨¬z)∨(¬x∨t)".match(reg2); 
    
   // rez.innerHTML=str+'≡<p>≡'+str1;
  return str1;//arr;  
}

function ne(A){ // реализуем '¬'
if (A[0]== '¬') 
{return A.substring(1);}
else {return '¬' + A;}
}


function lDoSk (str){

   // в str находим левый операнд. Отрицание не ставим. Возвращаем левый операнд.
   // Если крайний правый символ ')' находим слева соответствующую '('. 
   // Если Если крайний правый символ не ')', то операнд - переменная с '¬' или без.
   //


//   str='(((¬(¬(xxxx)))';
//      str='¬x';
   
   var i=str.length-2, sk=1;
   if (str[i+1]!=')') {
       return str[i]=='¬'?str[i]+str[i+1]:str[i+1];
       }
   
   while (!(str[i]=='(' && sk==1)){
   
    if (str[i]=='(') {sk--;}
    if (str[i]==')') {sk++;}
    i--;
    if (i<0) {return 'нет ('}
   }
   var ch=str[i];
   var str2=str.substring(i);
   if (i>0 && str[i-1]=='¬'){
     str2=str.substring(i-1);
   } else {
     str2=str.substring(i);
   }
  return str2; 
}

function rDoSk (str){

   // в str находим правый операнд. Отрицание не ставим. Возвращаем правый операнд.
   // Если крайние левые символы '¬(' или '(' находим справа соответствующую ')'. 
   // Если Если крайний правый символ не '(', то операнд - переменная с '¬' или без.
   //

   var i=0, sk=1;
   if (str[i]=='¬') {i=1;}
   if (str[i]!='(') {
       return str.substr(0,i+1);
       }
   var l=str.length;
   i++;
   while (!(str[i]==')' && sk==1)){
   
    if (str[i]==')') {sk--;}
    if (str[i]=='(') {sk++;}
    i++;
    if (i>l) {return 'нет )'}
   }
   var ch=str[i];
   var str2=str.substring(0,i+1);
   return str2; 
}

function lDoSkOtr(str){

   // в str находим левый операнд. Если крайний правый символ ')' находим слева
   // соответствующую '('. Если перед ней нет '¬' - ставим. Если есть - убираем.
   // Если Если крайний правый символ не ')', то операнд - переменная с '¬' или без.
   // Если нет '¬' - ставим. Если есть - убираем.


// str='(((¬(¬(xxxx)';
   var str1; 
   var i=str.length-2, sk=1;
   if (str[i+1]!=')') {
     if (str[0]=='¬') {str1=str[1];} else {str1='¬'+str;}
     return str1;
     }
   
   while (!(str[i]=='(' && sk==1)){
   
    if (str[i]=='(') {sk--;}
    if (str[i]==')') {sk++;}
    i--;
    if (i<0) {return 'нет ('}
   }
   var ch=str[i];
   var str2=str.substring(i);
   if (i>0 && str[i-1]=='¬'){
     str1=str.substring(0,i-1)+str2; 
   } else {
      str1=str.substring(0,i)+'¬'+str2; 
   }
  return str1; 
}

function insertFunc(){
 var formula=SpreadsheetApp.getActive().getRange('P2').getValue();
 var html=HtmlService.createTemplateFromFile('преобразитель');
 html.a=formula;
 var t=html.evaluate().setHeight(100).setWidth(500);
 
 //var html=HtmlService.createHtmlOutputFromFile('copyFI').setHeight(150).setWidth(350);
 SpreadsheetApp.getUi()
//       .showSidebar(html)
      .showModalDialog(t, 'Логический преобразитель');
 
}