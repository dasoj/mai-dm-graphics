const n = 10
X = [1, 2, 3, 4]
Xn = 4

function clearAnswer(){
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Задача 2');
  var arr = new Array(Xn).fill(['']);
  sheet.getRange(3, 4, Xn, 1).setValues(arr);
  sheet.getRange(3, 6, Xn, 1).setValues(arr);
  var arr = new Array(n).fill(['','']);
  sheet.getRange(3, 8, n, 2).setValues(arr);
  var arr = new Array(n*Xn).fill(['','']);
  sheet.getRange(3, 11, n*Xn, 2).setValues(arr);
  sheet.getRange(15, 15, 4, 1).setValues([[''], [''], [''], ['']]);
  clearPictures()
}

function clearRelation(){
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Задача 2');
  var arr = new Array(n).fill(['','']);
  sheet.getRange(3, 1, n, 2).setValues(arr);
}

function clearPictures() {
  SpreadsheetApp.getActive().getSheetByName('Задача 2').getRange('A25').setValue('');
  SpreadsheetApp.getActive().getSheetByName('Задача 2').getRange('E25').setValue('');
  SpreadsheetApp.getActive().getSheetByName('Задача 2').getRange('H25').setValue('');
}

function generateRelation() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Задача 2');
  var relation = sheet.getRange(3, 1, n, 2).getValues();
  m=(Math.floor(Math.random() * 5) + 5)
  for (var i = 0; i < m; i++) {
    x = Math.floor(Math.random() * 4) + 1
    y = Math.floor(Math.random() * 4) + 1
    relation[i]=[x,y]
  }
  for (var i=m;i<n;i++){
    relation[i]=[Infinity, Infinity]
  }
  sheet.getRange(3, 1, n, 2).setValues(format(relation,n, 2));
}
function find() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Задача 2');
  var relation = sheet.getRange(3, 1, n, 2).getValues();
  for (var i=0; i<n; i++){
    x = Number(relation[i][0]);
    y = Number(relation[i][1]);
    relation[i] = [(x==0)?Infinity:x, (y==0)?Infinity:y];
  }
  sheet.getRange(3, 4, Xn, 1).setValues(getD(relation));
  sheet.getRange(3, 6, Xn, 1).setValues(getR(relation));
  sheet.getRange(3, 8, n, 2).setValues(getInversion(relation));
  composion = getComposition(relation)
  sheet.getRange(3, 11, composion.length, 2).setValues(composion);
  sheet.getRange(3, 1, n, 2).setValues(format(relation, n, 2));
}

function format(A, h, l){ // осторожно, функция меняет A 
//Browser.msgBox([A,'h',h,'l', l]);
  A.sort()
  for (var i=1; i<A.length; i++){
    //if (h==n*Xn) Browser.msgBox([A[i-1], ' - ',A[i]])
    if ((A[i][0] == A[i-1][0])&&(A[i][1] == A[i-1][1])){
      //Browser.msgBox(A[i]);
      if (l==2){
        A[i-1] = [Infinity, Infinity]
      }
    }
  }
  A.sort()
  for (var i=0; i<A.length; i++){   
    if (A[i][0] == Infinity) A[i][0] = '';
    if (l>1){
      if (A[i][1] == Infinity) A[i][1] = '';
    }
  }
  return A;
}

function getD(relation){
  var d = [new Array(Xn)];
  for (var i=0; i<Xn; i++)d[i] = [Infinity];
  for (var i=0; i<n; i++){
    x = relation[i][0];
    if (relation[i][0] != 0) d[x-1]=[x];
  }
  return format(d, Xn,1)
}

function getR(relation){
  var r = [new Array(Xn)];
  for (var i=0; i<Xn; i++) r[i] = [Infinity];
  for (var i=0; i<n; i++){
    y = relation[i][1];
    if (y != 0) r[y-1]=[y];
  }
  return format(r, Xn, 1)
}

function getInversion(relation){
  var inversion = new Array(2);
  inversion[0] = new Array(n); 
  inversion[1] = new Array(n);
  var x=0;
  var y=0;
  for (var i=0; i<n; i++){
    [x,y] = relation[i];
    inversion[i] = [y, x];
  }
  return format(inversion, n, 2);
}

function getComposition(relation){
  var composition = new Array(2);
  composition[0] = new Array(n*Xn); 
  composition[1] = new Array(n*Xn);
  var k=0;
  for (var i=0;i<n;i++) {
    [x,y1] = relation[i];
    if (x==0) break;
    for (var j=0;j<n;j++) {
      [y2,z] = relation[j];
      if (z==0) break;
      if (y1==y2){
        composition[k] = [x, z];
        k++;
      }
    }
  }
  //Browser.msgBox([k]);
  for (k; k<n*Xn; k++){
    composition[k] = [Infinity, Infinity];
  }
  //Browser.msgBox([n*Xn, k, composition.length, composition[0].length]);
  return format(composition, n*Xn, 2);
}

function isRelation(){
  find()
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Задача 2');
  var relation = sheet.getRange(3, 1, n, 2).getValues();
  var inversion = sheet.getRange(3, 8, n, 2).getValues();
  var composition = sheet.getRange(3, 11, n*Xn, 2).getValues();
  for (var i=0; i<n; i++){
    [x,y] = [Number(relation[i][0]),Number(relation[i][1])];
    relation[i] = [(x==0)?Infinity:x, (y==0)?Infinity:y];
  }
  for (var i=0; i<n; i++){
    [x,y] = [Number(inversion[i][0]),Number(inversion[i][1])];
    inversion[i] = [(x==0)?Infinity:x, (y==0)?Infinity:y];
  }
  for (var i=0; i<n*Xn; i++){
    [x,y] = [Number(composition[i][0]),Number(composition[i][1])];
    composition[i] = [(x==0)?Infinity:x, (y==0)?Infinity:y];
  }
  [x,y] = isReflexive(relation);
  var str1=(x==Infinity)?'Да, т.к. ∀x∈X <x,x>∈ρ':'Нет, т.к. например <'+[x,y]+'>∉ρ';
  [x,y] = isSymmetric(relation, inversion);
  var str2=(x==Infinity)?'Да, т.к. ∀<x,y>∈ρ ⇒ <y,x>∈ρ':'Нет, т.к. например <'+[x,y]+'>∈ρ,но <'+[y,x]+'>∉ρ';
  [x,y] = isAntisymmetric(relation, inversion);
  var str3=(x==Infinity)?'Да, т.к. если <x,y>∈ρ и <y,x>∈ρ, то x=y':'Нет, т.к. например <'+[x,y]+'>∈ρ и <'+[y,x]+'>∈ρ';
  [x,y] = isTransitive(relation, composition);
  var str4=(x==Infinity)?'Да, т.к. ρ∘ρ⊆ρ':'Нет, т.к. например <'+[x,y]+'>∈ρ∘ρ но <'+[x,y]+'>∉ρ';
  sheet.getRange(15, 15, 4, 1).setValues([[str1], [str2], [str3], [str4]]);
  drawRelation()
}

function isReflexive(relation){
  var existxx = new Array(Xn);
  for (var i=0; i<Xn; i++) existxx[i] = false;
  for (var i=0; i<n; i++) {
    [x,y] = relation[i];
    if (x==0) break;
    if (x==y) existxx[x-1] = true;
  }
  for (var i=0; i<Xn; i++) if(!existxx[i]) return [i+1, i+1];
  return [Infinity, Infinity];
}

function isSymmetric (relation, inversion){
  for (var i=0; i<n; i++) {
    [x1,y1] = relation[i];
    [x2, y2] = inversion[i];
    if (x1==0) break;
    if ((x1!=x2)||(y1!=y2)) return [x1, y1];
  }
  return [Infinity, Infinity];
}

function isAntisymmetric (relation, inversion){
  for (var i=0; i<n; i++) {
    [x1,y1] = relation[i];
    if (x1==0) break;
    for (var j=0; j<n; j++) {
      [x2, y2] = inversion[j];
      if (x1 == y1) continue
      if ((x1==x2)&&(y1==y2)) return [x1, y1];
    }
  }
  return [Infinity, Infinity];
} 

function isTransitive (relation, composition){
  for (var i=0; i<n*Xn; i++) {
    [x1,y1] = composition[i];
    var inRelation = false
    if (x1==0) break;
    for (var j=0; j<n; j++) {
      [x2, y2] = relation[j];
      if ((x1==x2)&&(y1==y2)) inRelation=true;
    }
    if (!inRelation) return [x1, y1]
  }
  return [Infinity, Infinity];
} 

 function drawRelation(){
  var type='fdp';
  Logger.log('nolabel=1');
  Logger.log('shape= circle');
  var defis='->';
  var defis1='--'
  var rankdir='rankdir=RL;';
  Logger.log('lsk= ');
  var prefix='https://chart.googleapis.com/chart?cht=gv:'+type+'&chl=';
  var prefix1=prefix.replace(type,'dot');
  var prefix2='https://chart.googleapis.com/chart?cht=s&chd=t:';
  var chl='digraph{'+rankdir+'edge [style=single] '+'node [style=single, shape=circle] ';
  var chl1='graph {'+rankdir+'edge [style=single] '+'node [style=single, shape=circle] ';
  chl1 = chl1+'rank=same{x1--x2--x3--x4[color=white]};rank=same{y1--y2--y3--y4[color=white]}';
  chl  = chl +'1;2;3;4;';
  var postfix='}&chs=400x400';
  var postfix2='&chm=o,000000,0,,20&chxt=x,y&chxl=0:||1|2|3|4||1:||1|2|3|4|&chds=0,5&chxs=0,000000,40,0,lt|1,000000,40,0,lt&chs=400x400'
  var out='';
  var out1='';
  var outx='';
  var outy='';

  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Задача 2');
  var relation = sheet.getRange(3, 1, n, 2).getValues();
  var A=[[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
  for (var i=0; i<n; i++){
    [x,y] = [Number(relation[i][0]),Number(relation[i][1])];
    relation[i] = [(x==0)?Infinity:x, (y==0)?Infinity:y];
    [x,y] = relation[i];
    if (x!=Infinity) {A[x-1][y-1]=1}
  }  

  for (var i=0;i<A.length;i++){
    tekVer=''+(i+1)
    tekVer1='x'+(i+1);
    for (var j=0;j<A[i].length;j++){
      if(A[i][j]!=0){
        out+=tekVer+defis+(j+1)+';';
        out1+='y'+(j+1)+defis1+tekVer1+';';
        if (outx != ''){
          outx+=','
          outy+=','
        }
        outx+=(i+1)
        outy+=(j+1)
      }
    }
  }
  var urlE=encodeURI(prefix+chl+out+postfix);
  var urlE1=encodeURI(prefix1+chl1+out1+postfix);
  var urlE2=encodeURI(prefix2+outx+'|'+outy+postfix2);

  SpreadsheetApp.getActive().getSheetByName('Задача 2').getRange('E25').setValue('=IMAGE("'+urlE+'")');

  SpreadsheetApp.getActive().getSheetByName('Задача 2').getRange('H25').setValue('=IMAGE("'+urlE1+'")');

  SpreadsheetApp.getActive().getSheetByName('Задача 2').getRange('A25').setValue('=IMAGE("'+urlE2+'")');
}