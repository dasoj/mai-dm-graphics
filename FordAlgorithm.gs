// реализация Алгоритма Форда-Белмана в Гугл таблицах

const n = 8;     // размер входящей матрицы смежности. Можно легко поменять и программа подстроиться
var pref = new Array(n); // нужен для вывода кратчайшего пути

function ford_belman() {
  var ss = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('ДМ')
  var graph = ss.getSheetValues(3, 3, n, n)
  var INF = '∞'
  var infinity = 100;
  for (var i = 0; i < n; i++) {
    for (var j = 0; j < n; j++) {
      if (graph[i][j] == INF || graph[i][j] == '*') {
        graph[i][j] = infinity          // если я встретил символ бесконечности, то меняю его на 100 (заведомо больший путь)
      } 
    }
  }

  var res = ss.getSheetValues(3, 14, n, n) 
  for (var i = 0; i < n; i++) {
    for (var j = 0; j < n; j++) {
      res[i][j] = infinity;             // заполняю матрицу бесконечностями
    }
  }
  res[0][0] = 0;
  pref[0] = 0;
  for (var k = 1; k < n; k++) {
    for (var i = 0; i < n; i++) {
      res[k][i] = res[k - 1][i]
      for (var j = 0; j < n; j++) {
        if (res[k - 1][j] + graph[j][i] < res[k][i]) {
          res[k][i] = res[k - 1][j] + graph[j][i]                // Алгоритм Форда-Белмана
          pref[i] = j;        // запоминаю вершину, из которой пришел
        }
      }
    }
  }
  for (var i = 0; i < n; i++) {
      for (var j = 0; j < n; j++) {
        if (res[i][j] == infinity) {
          res[i][j] = INF;
        }
      }
  }

  return res;
}

function full_answer() {
  var ss = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('ДМ');
  var title = new Array(n)
  for (var i = 0; i < n; i++) {
    title[i] = i;
  }
  ss.getRange(2, 14, 1, n).setValues([title]);
  res = ford_belman();
  var a; 

  // транспонирую матрицу
  for (var i = 0; i < n; i++) {
    for (var j = i; j < n; j++) {
      a = res[i][j];
      res[i][j] = res[j][i];
      res[j][i] = a;
    }
  }

  ss.getRange(3, 14, n, n).setValues(res);
}

function generate_graph() {
  var ss = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('ДМ');
  var graph = ss.getSheetValues(3, 3, n, n);
  var INF = '∞';
  for (var i = 0; i < n; i++) {
    for (var j = 0; j < n; j++) {
      if (i == j) {
        graph[i][j] = INF;
      } else {
        graph[i][j] = Math.floor(Math.random() * 20) + 1; //random выдает число [0;1) Хочу получить числа [1;14]
        if (graph[i][j] >= 15) {        // если число >= 10 меняю на бесконечность
          graph[i][j] = INF;
        }
      }
    }
  }
  ss.getRange(3, 3, n, n).setValues(graph);
}

function iter() {
  var ss = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('ДМ');
  var lastcol = ss.getLastColumn();
  var firstcol = 14;
  if (lastcol == 10) {
    var column = 14;
  } else {
    var column=lastcol + 1;
  }
  var res = ford_belman();
  ss.getRange(2, column).setValue(column - firstcol);
  for (var i = 0; i < n; i++) {
    ss.getRange(3 + i, column).setValue(res[column - firstcol][i]);
  }
}

function print_way() {
  var ss = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('ДМ');
  var res = ford_belman();    // чтобы массив pref заполнился 
  Logger.log(pref);
  for (var i = 1; i < n; i++) {
    var u = i;
    var way = [];
    while(1) {
        if (u == 0) {
            way.push(u + 1);
            break;
        }
        way.push(u + 1);
        u = pref[u];
    }
    way = way.reverse();
    var path = "";
    for (var j= 0; j < way.length; j++) {
      path = path + "v" + String(way[j]) + " ";
    }
    ss.getRange(3 + i, 24).setValue(path);
  }
  
}



