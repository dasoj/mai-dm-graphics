function onOpen() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = spreadsheet.getActiveSheet();
  var initial = sheet.getRange('A3:B3');
  initial.setValue(0);
}




function drawChart() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = spreadsheet.getActiveSheet();

  var dataToClear = sheet.getRange('A9:H18');
  dataToClear.clearContent();

  var lineDataRange = sheet.getRange('C3:C8');
  lineDataRange.setValue(0);

  var dataRange = sheet.getRange('A3:H18');
  var chart = sheet.newChart()
    .setChartType(Charts.ChartType.LINE)
    .addRange(dataRange)
    .setPosition(2, 3, 0, 0)
    .setOption('vAxes.0.viewWindow.min', 0)
    .setOption('vAxes.0.viewWindow.max', 4)
    .build();

  sheet.insertChart(chart);

}


function lineUp() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = spreadsheet.getActiveSheet();

  var dataToClear = sheet.getRange('A9:H18');
  dataToClear.clearContent();

  var lineDataRange = sheet.getRange('C3:C8');
  var lineValue = lineDataRange.getValue() + 0.5;
  lineDataRange.setValue(lineValue);

  var dataRange = sheet.getRange('A3:B8');
  var data = dataRange.getValues();
  var counter = 1;
  for(var i = 0; i < data.length; i++) {
    var segmentStartX = data[i][0];
    var segmentStartY = data[i][1];

    var nextSegmentIndex = (i + 1) % data.length;
    var segmentEndX = data[nextSegmentIndex][0];
    var segmentEndY = data[nextSegmentIndex][1];
    if (segmentEndX == "" || segmentEndY == "") break;

    var minX = Math.min(segmentStartX, segmentEndX);
    var minY = Math.min(segmentStartY, segmentEndY);
    var maxX = Math.max(segmentStartX, segmentEndX);
    var maxY = Math.max(segmentStartY, segmentEndY);

    if (lineValue >= minY && lineValue <= maxY) {
      var intersectionX = segmentStartX + (lineValue - segmentStartY) * (segmentEndX - segmentStartX) / (segmentEndY - segmentStartY);
      if (intersectionX >= minX && intersectionX <= maxX) {
        if (counter == 1) {
          var rangeX = sheet.getRange('A9:A10');
          var rangeUpY = sheet.getRange('D9');
          var rangeDownY = sheet.getRange('D10');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        else if (counter == 2) {
          var rangeX = sheet.getRange('A11:A12');
          var rangeUpY = sheet.getRange('E11');
          var rangeDownY = sheet.getRange('E12');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        else if (counter == 3) {
          var rangeX = sheet.getRange('A13:A14');
          var rangeUpY = sheet.getRange('F13');
          var rangeDownY = sheet.getRange('F14');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        else if (counter == 4) {
          var rangeX = sheet.getRange('A15:A16');
          var rangeUpY = sheet.getRange('G15');
          var rangeDownY = sheet.getRange('G16');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        else if (counter == 5) {
          var rangeX = sheet.getRange('A17:A18');
          var rangeUpY = sheet.getRange('H17');
          var rangeDownY = sheet.getRange('H18');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        counter += 1;
      }
    }

  }

}


function lineDown() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = spreadsheet.getActiveSheet();

  var dataToClear = sheet.getRange('A9:H18');
  dataToClear.clearContent();

  var lineDataRange = sheet.getRange('C3:C8');
  var lineValue = lineDataRange.getValue() - 0.5;
  lineDataRange.setValue(lineValue);

  var dataRange = sheet.getRange('A3:B8');
  var data = dataRange.getValues();
  var counter = 1;
  for(var i = 0; i < data.length; i++) {
    var segmentStartX = data[i][0];
    var segmentStartY = data[i][1];

    var nextSegmentIndex = (i + 1) % data.length;
    var segmentEndX = data[nextSegmentIndex][0];
    var segmentEndY = data[nextSegmentIndex][1];
    if (segmentEndX == "" || segmentEndY == "") break;

    var minX = Math.min(segmentStartX, segmentEndX);
    var minY = Math.min(segmentStartY, segmentEndY);
    var maxX = Math.max(segmentStartX, segmentEndX);
    var maxY = Math.max(segmentStartY, segmentEndY);

    if (lineValue >= minY && lineValue <= maxY) {
      var intersectionX = segmentStartX + (lineValue - segmentStartY) * (segmentEndX - segmentStartX) / (segmentEndY - segmentStartY);
      if (intersectionX >= minX && intersectionX <= maxX) {
        if (counter == 1) {
          var rangeX = sheet.getRange('A9:A10');
          var rangeUpY = sheet.getRange('D9');
          var rangeDownY = sheet.getRange('D10');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        else if (counter == 2) {
          var rangeX = sheet.getRange('A11:A12');
          var rangeUpY = sheet.getRange('E11');
          var rangeDownY = sheet.getRange('E12');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        else if (counter == 3) {
          var rangeX = sheet.getRange('A13:A14');
          var rangeUpY = sheet.getRange('F13');
          var rangeDownY = sheet.getRange('F14');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        else if (counter == 4) {
          var rangeX = sheet.getRange('A15:A16');
          var rangeUpY = sheet.getRange('G15');
          var rangeDownY = sheet.getRange('G16');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        else if (counter == 5) {
          var rangeX = sheet.getRange('A17:A18');
          var rangeUpY = sheet.getRange('H17');
          var rangeDownY = sheet.getRange('H18');
          rangeX.setValue(intersectionX);
          rangeUpY.setValue(lineValue);
          rangeDownY.setValue(0);
        }
        counter += 1;
      }
    }

  }

}


