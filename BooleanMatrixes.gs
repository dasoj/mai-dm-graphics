function xxx(A) {  
var z=Ematrix(4);
return 1;
}


function Ematrix(n) {  // единичная матрица n на n
// var B=A.map (function (ai){return ai.map(function(aij){if (aij!=0)return 1;return aij; });})
 
var B = []; //new BoolArr;
for (i=0;i<n;i++)
{B[i]=[];
for (j=0;j<n;j++)
if (i==j) {B[i][j]=1;}
else {B[i][j]=0;}}
return B;    
}

function bool(A) {  // если не 0 будет 1
// работает, но сложно понять  var B=A.map (function (ai){return ai.map(function(aij){if (aij!=0)return 1;return aij; });})
var B=[];
for (i=0;i<A.length;i++)
{B[i]=[];
for (j=0;j<A.length;j++)
if (A[i][j]!==0) {B[i][j]=1;}
else {B[i][j]=0;}}
return B;    
}

function andMatrix(A,B)  // поэлементная конъюнкция
{   
    var m = A.length, n = A[0].length, C = [];
    for (var i = 0; i < m; i++)
     { C[i] = [];
       for (var j = 0; j < n; j++) C[i][j] = A[i][j]*B[i][j];
     }
    return C;
}

function orMatrix(A,B)  // поэлементная дизъюнкция
{   
    var m = A.length, n = A[0].length, C = [];
    for (var i = 0; i < m; i++)
     { C[i] = [];
       for (var j = 0; j < n; j++) C[i][j] = Math.max(A[i][j],B[i][j]);
     }
    return C;
}

function writeS(name,arr,row,col){
 var N=arr.length;
 var n=arr[0].length;
 A=SpreadsheetApp.getActive().getSheetByName(name).getRange(row,col, N, n).setValues(arr);

}

function toglleX(){
 var c=SpreadsheetApp.getActiveSheet().getActiveCell()
 if (c.getValue()==0) c.setValue(1);
 else c.setValue(0);   
}

function toglleList(){
// var list=SpreadsheetApp.getActiveRangeList();
 let A1=[];
// list.setValue(1);
// list.forEach((r)=>{A1.push(r.getA1Notation())});
 var c=SpreadsheetApp.getActiveSheet().getActiveCell();
 if (c.getValue()==0) c.setValue(1);
 else c.setValue(0);   
}

function nomer7(){
 
 var A=[], A2s=[], A2=[],A3=[],A3s=[], S=[],E=Ematrix(4);
 var tekCell=SpreadsheetApp.getActive().getSheetByName('задача 7').getActiveCell().activateAsCurrentCell();
 A=SpreadsheetApp.getActive().getSheetByName('задача 7').getRange(2, 2, 4, 4).getValues();
 A2=MultiplyMatrix(A,A);
 writeS('задача 7',A2,7,2);
 A2s=bool(A2);
 A3=MultiplyMatrix(A2,A);
 writeS('задача 7',A3,12,2);
 A3s=bool(A3);
 var D=[];//new BoolArr();

// D= orMatrix(orMatrix(orMatrix(E,A),A2s),A3s); //;
 D = E.or(A).or(A2s).or(A3s);
 writeS('задача 7',D,17,2);
 S= andMatrix(D,TransMatrix(D));
 writeS('задача 7',S,22,2);
 gv();
 calcVnut();
 calcVnesh();
 
 }
 
 function nomer7_null(){
 writeS('задача 7',[[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]],2,2);
 writeS('задача 7',[['','','',''],['','','',''],['','','',''],['','','','']],7,2);
 writeS('задача 7',[['','','',''],['','','',''],['','','',''],['','','','']],12,2);
 writeS('задача 7',[['','','',''],['','','',''],['','','',''],['','','','']],17,2);
 writeS('задача 7',[['','','',''],['','','',''],['','','',''],['','','','']],22,2);
 writeS('задача 7',[['']],2,7);
 writeS('задача 7',[['']],10,7);
 writeS('задача 7',[['']],19,7);
 writeS('задача 7',[['']],23,7);
 }
 

 
 function gv(gr,type,shape,nagr,size,napr,skobki, nolabel){
   gr=gr||'digraph';
   type=type||'circo'
   shape=shape||'circle';
   size=size||'400x400';
   napr =napr||'RL';
   skobki = skobki||0;
   nolabel=1;
   Logger.log('nolabel= '+nolabel);
   Logger.log('shape= '+shape);
   var defis=(gr=='digraph')?'->':'--';
   var rankdir='rankdir='+napr+';';
   var lsk=(skobki==1)?'(':'';
   var psk=(skobki==1)?')':'';
   Logger.log('lsk= '+lsk);
   var prefix='https://chart.googleapis.com/chart?cht=gv:'+type+'&chl=';
   var chl=gr+'{'+rankdir+'edge [style=single] '+
        'node [style=single, shape='+shape+'] ';
   var postfix='}&chs='+size;
   var out='';
   var out0='';
   var A=SpreadsheetApp.getActive().getSheetByName('задача 7').getRange(2, 2, 4, 4).getValues();
   var cluster=kompSvjaz();
   var clusters='';
   for (var i=0;i<cluster.length;i++){
     clusters +=' subgraph cluster'+i+' {'+cluster[i].join(',')+' label=C'+(i+1)+'};'
   }
   var prefix1=prefix.replace('circo','fdp');
   var chl1=chl+clusters;
   for (var i=0;i<A.length;i++){
   if (shape=='point'&&nolabel==0){
    out+=i+1+' [xlabel=<v<sub>'+(i+1)+'</sub>>];';
    out0+=i+1+' [xlabel=v'+(i+1)+'];';}
   else {
    out+=i+1+' [label=<v<sub>'+(i+1)+'</sub>>];';
    out0+=i+1+' [label=v'+(i+1)+'];';}
  }
  for (var i=0;i<A.length;i++){
     tekVer=''+(i+1);
     for (var j=(gr=='digraph')?0:i;j<A[i].length;j++){
      if(A[i][j]!=0){
        var label=(nagr==1)?'[label="'+lsk+A[i][j]+psk+'"'+']':'';
        out+=tekVer+defis+(j+1)+label+';';
        out0+=tekVer+defis+(j+1)+label+';';}
      }
      
  
}
var url=prefix+chl+out0+postfix;
var urlE=encodeURI(url);
var urlE1=encodeURI(prefix1+chl1+out0+postfix);

SpreadsheetApp.getActive().getSheetByName('задача 7').getRange('G2').setValue('=IMAGE("'+urlE+'")');

SpreadsheetApp.getActive().getSheetByName('задача 7').getRange('G10').setValue('=IMAGE("'+urlE1+'")');
 }
 

function rndMatrix(){
   
   var A=[];
   for (var j=0; j<4;j++){
   var arr=[];
   for (var i=0; i<4;i++){
    var x=rndRange(0,1);
    arr.push(x);
    }
    arr[j]=0;
    A.push(arr);
    }
   writeS('задача 7',A,2,2); 
     
  } 
 
 
function kompSvjaz(D){
  D=SpreadsheetApp.getActive().getSheetByName('задача 7').getRange(22, 2, 4, 4).getValues();
  N=D.length;
  var fltr=[];
  for (var j=0; j<N;j++){
  fltr.push(true);
  for (var i=0; i<N;i++){
    if (D[i][j]){
    D[i][j]=j+1;}
    }
 
    }
    
    var cluster=[];
    var ii=-1;
    for (var i=0; i<N;i++){
     if (fltr[i]){
      cluster.push(D[i].filter(function(d,i){return (fltr[i]&&d>0)}));
      ii++;
      for (var j=0;j<cluster[ii].length;j++){
        fltr[cluster[ii][j]-1]=false;
     } 
    }
    }
    a=2
    return cluster;
}