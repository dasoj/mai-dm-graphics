function TransMatrix(A) { //На входе двумерный массив
  var m = A.length, n = A[0].length, AT = [];
  for (var i = 0; i < n; i++){ 
    AT[i] = [];
    for (var j = 0; j < m; j++) AT[i][j] = A[j][i];
  }
  return AT;
}


function MultiplyMatrix(A,B) {
  var rowsA = A.length, colsA = A[0].length,
      rowsB = B.length, colsB = B[0].length,
      C = [];
  if (colsA != rowsB) return false;
  for (var i = 0; i < rowsA; i++) C[i] = [];
  for (var k = 0; k < colsB; k++){ 
    for (var i = 0; i < rowsA; i++){ 
      var t = 0;
      for (var j = 0; j < rowsB; j++) t += A[i][j]*B[j][k];
      C[i][k] = t;
    }
  }
  return C;
}


function MatrixPow(n,A) { 
  if (A.length!=A[0].length) return false;
  A2 = MatrixPow(Math.floor(n/2), A);
  A2 = MultiplyMatrix(A2, A2);
  if (n % 2 != 0) A2 = MultiplyMatrix(A2, A);
  return A2;
}


function Ematrix(n) {  // единичная матрица n на n
  var E = []; //new BoolArr;
  for (i=0;i<n;i++){
    E[i]=[];
    for (j=0;j<n;j++)
      if (i==j) {E[i][j]=1;}
      else {E[i][j]=0;}
  }
  return E;    
}


function bool(A) {  // если не 0 будет 1
  var B=[];
  for (i=0;i<A.length;i++){
    B[i]=[];
    for (j=0;j<A[0].length;j++)
      if (A[i][j]!==0) {B[i][j]=1;}
      else {B[i][j]=0;}
  }
  return B;    
}


function andMatrix(A,B) { // поэлементная конъюнкция  
  var rowsA = A.length, colsA = A[0].length, C = [];
  if ((rowsA != B.length)||(colsA != B[0].length)) return false;
  for (var i = 0; i < rowsA; i++){ 
    C[i] = [];
    for (var j = 0; j < colsA; j++) C[i][j] = A[i][j]&B[i][j];
  }
  return C;
}


function orMatrix(A,B) { // поэлементная дизъюнкция
  var rowsA = A.length, colsA = A[0].length, C = [];
  if ((rowsA != B.length)||(colsA != B[0].length)) return false;
  for (var i = 0; i < rowsA; i++){ 
    C[i] = [];
    for (var j = 0; j < colsA; j++) C[i][j] = A[i][j]|B[i][j];
  }
  return C;
}


function toglleX(){
  var c=SpreadsheetApp.getActiveSheet().getActiveCell()
  if (c.getValue()==0) c.setValue(1);
  else c.setValue(0);   
}

function toglleList(){
  var rangeList=SpreadsheetApp.getActiveSheet().getActiveRangeList().getRanges();
  for (var k=0; k<rangeList.length;k++){
    A=rangeList[k].getValues();
    for (var i=0; i<A.length;i++){
      for (var j=0; j<A[0].length;j++){
        A[i][j] = (A[i][j]==0)?1:0;
      }
    }
    rangeList[k].setValues(A);
  }
}


function writeS(name,arr,row,col){
  var N=arr.length;
  var n=arr[0].length;
  A=SpreadsheetApp.getActive().getSheetByName(name).getRange(row,col, N, n).setValues(arr);
}


function clear(){
  writeS('задача 7',[[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]],2,2);
  writeS('задача 7',[['','','',''],['','','',''],['','','',''],['','','','']],7,2);
  writeS('задача 7',[['','','',''],['','','',''],['','','',''],['','','','']],12,2);
  writeS('задача 7',[['','','',''],['','','',''],['','','',''],['','','','']],17,2);
  writeS('задача 7',[['','','',''],['','','',''],['','','',''],['','','','']],22,2);
  writeS('задача 7',[['']],1,8);
  writeS('задача 7',[['']],6,8);
  writeS('задача 7',[['']],11,8);
  writeS('задача 7',[['']],2,7);
  writeS('задача 7',[['']],10,7);
  writeS('задача 7',[['']],19,7);
  writeS('задача 7',[['']],23,7);
}


function rndMatrix(n=4){
  var A=new Array(n);
  for (var i=0; i<n;i++){
    A[i]=new Array(n);
    for (var j=0; j<n;j++){
      A[i][j]=Math.floor(Math.random()*(2));
    }
    A[i][i]=0;
  }
  writeS('задача 7',A,2,2); 
} 

function solve(){
  var A=[], A2s=[], A2=[],A3=[],A3s=[], S=[],E=Ematrix(4);
  A=SpreadsheetApp.getActive().getSheetByName('задача 7').getRange(2, 2, 4, 4).getValues();
  var iscont=A[0][0]+A[1][1]+A[2][2]+A[3][3]>0
  writeS('задача 7',[[(iscont)?'есть контуры длины 1':'нет контуров длины 1']],1,8);

  A2=MultiplyMatrix(A,A);
  writeS('задача 7',A2,7,2);
  iscont=A2[0][0]+A2[1][1]+A2[2][2]+A2[3][3]>0
  writeS('задача 7',[[(iscont)?'есть контуры длины 2':'нет контуров длины 2']],6,8);
  A2s=bool(A2);

  A3=MultiplyMatrix(A2,A);
  writeS('задача 7',A3,12,2);
  iscont=A3[0][0]+A3[1][1]+A3[2][2]+A3[3][3]>0
  writeS('задача 7',[[(iscont)?'есть контуры длины 3':'нет контуров длины 3']],11,8);
  A3s=bool(A3);

  var D=[];
  D = orMatrix(E, orMatrix(A, orMatrix(A2s, A3s)));
  writeS('задача 7',D,17,2);
  S = andMatrix(D, TransMatrix(D));
  writeS('задача 7',S,22,2);
  gv();
  calcIn();
  calcOut();
}


function gv(gr='digraph',type='circo',shape='circle',weght=0,size='400x400',direction='RL',brackets=0, nolabel=1){
  var defis=(gr=='digraph')?'->':'--';
  var rankdir='rankdir='+direction+';';
  var lsk=(brackets==1)?'(':'';
  var psk=(brackets==1)?')':'';
  var prefix='https://chart.googleapis.com/chart?cht=gv:'+type+'&chl=';
  var chl=gr+'{'+rankdir+'edge [style=single] node [style=single, shape='+shape+'] ';
  var postfix='}&chs='+size;
  var out='';
  var A=SpreadsheetApp.getActive().getSheetByName('задача 7').getRange(2, 2, 4, 4).getValues();
  
  var cluster=connectComponents();
  var clusters='';
  for (var i=0;i<cluster.length;i++){
    clusters +=' subgraph cluster'+i+' {'+cluster[i].join(',')+' label=C'+(i+1)+'};'
  }
  var prefix1=prefix.replace('circo','fdp');
  var chl1=chl+clusters;
  for (var i=0;i<A.length;i++){
  if (shape=='point'&&nolabel==0){
    out+=i+1+' [xlabel=v'+(i+1)+'];';}
  else {
    out+=i+1+' [label=v'+(i+1)+'];';}
  }
  for (var i=0;i<A.length;i++){
      for (var j=(gr=='digraph')?0:i;j<A[i].length;j++){
      if(A[i][j]!=0){
        var label=(weght==1)?'[label="'+lsk+A[i][j]+psk+'"'+']':'';
        out+=(i+1)+defis+(j+1)+label+';';}
    }  
  }

  var urlE=encodeURI(prefix+chl+out+postfix);
  var urlE1=encodeURI(prefix1+chl1+out+postfix);
  
  writeS('задача 7', [['=IMAGE("'+urlE+'")']], 2, 7);
  writeS('задача 7', [['=IMAGE("'+urlE1+'")']], 10, 7);
}


function connectComponents(S){
  S=SpreadsheetApp.getActive().getSheetByName('задача 7').getRange(22, 2, 4, 4).getValues();
  N=S.length;
  var fltr=[];
  for (var j=0; j<N;j++){
    fltr.push(true);
    for (var i=0; i<N;i++){
      if (S[i][j]){S[i][j]=j+1;}
    }
  }
  
   
  var cluster=[];
  var ii=-1;
  for (var i=0; i<N;i++){
    if (fltr[i]){
      cluster.push(S[i].filter(function(d,i){return (fltr[i]&&d>0)}));
      ii++;
      for (var j=0;j<cluster[ii].length;j++){
        fltr[cluster[ii][j]-1]=false;
      } 
    }
  }
  return cluster;
}


function tablTrue(n){
  var arr1=[];
  var N=1<<n
  for (var i=0;i<N;i++){
    var arr2=[];
    for (var j=0;j<n;j++){
      arr2[j]=(i & (1 << (n-j-1)))===0?1:0;
    }
    arr1[i]=arr2;
  }
  return arr1; 
}

function notLessN(x,y,n){
  for (var i = 0; i < n; i++){
    if (x[i]<y[i]) return false;
  }
  return true; 
}

function calcIn(){
  var n=4;
  var A=SpreadsheetApp.getActive().getSheetByName('задача 7').getRange(2, 2, n, n).getValues();
  var strF='';
  for (var i=0;i<n;i++){
    for (var j=0;j<n;j++){
      if (A[i][j]) {strF+='(v['+i+'] || v['+j+'])&&'}
    }
  }
  strF+='1';
  var tableF=tablTrue(n);
  for (var i=0; i<tableF.length;i++){
    var v=tableF[i];
    tableF[i].push(eval(strF));
  }   // tableF таблица формулы для нахождения внутренне устойчивых множеств
   
  var tableIn=tableF.filter(function(v,i){return v[n]==1;}); 
   
  for (var i=tableIn.length-1;i>0;i--){
    var v=tableIn[i];
    for (var j=i-1;j>=0;j--){
      if (tableIn[j][n]==1){
        var x=tableIn[j];
        if (notLessN(x, v, n)) {
          tableIn[j][n]=0;
        }
      }
    }
  }
   
  //  в n-м столбце матрицы tableIn отмечены 1-цей максимальные внутренне устойчивые множества. Соответствуют 0-м.
  var tableIn=tableIn.filter(function(v,i){return v[n]==1;}); 
  tableIn.forEach(function(v){v.splice(n)});
   
  var arr= [];
  for (var i=0;i<tableIn.length;i++){
     tableIn[i].forEach(function(v,j){if(v==0){tableIn[i][j]='v'+(j+1)}});
    tableIn[i]=tableIn[i].filter (function(v){{return v!=1}});
    arr.push('{'+  tableIn[i].join()+'}');           
  }
  var str=arr.join();
  writeS('задача 7',[['максимальные внутренне устойчивые множества\n'+str]],19,7);
}

function calcOut(){
  var n=4;
  var A=SpreadsheetApp.getActive().getSheetByName('задача 7').getRange(2, 2, n, n).getValues();
  var strF='';
  for (var i=0;i<n;i++){
    strF+='(v['+i+'] ||';
    for (var j=0;j<n;j++){
      if (A[i][j]) {strF+=' v['+j+'] ||'}
    }
    strF+='0) &&';
  }
  strF+='1';
  var tableF=tablTrue(n);
  for (var i=0; i<tableF.length;i++){
    var v=tableF[i];
    tableF[i].push(eval(strF));
  }   // tableF таблица формулы для нахождения внешне устойчивых множеств
   
  var tableOut=tableF.filter(function(v,i){return v[n]==1;}); 
   
  for (var i=tableOut.length-1;i>0;i--){
    var v=tableOut[i];
    for (var j=i-1;j>=0;j--){
      if (tableOut[j][n]==1){
        var x=tableOut[j];
        if (notLessN(x, v, n)) {
          tableOut[j][n]=0;
        }
      }
    }
  }
   
  //  в n-м столбце матрицы tableIn отмечены 1-цей мининые внешне устойчивые множества. Соответствуют 1-цам.
  var tableOut=tableOut.filter(function(v,i){return v[n]==1;}); 
  tableOut.forEach(function(v){v.splice(n)});
   
  var arr= [];
  for (var i=0;i<tableOut.length;i++){
  // for (var j=0;j<n;j++){
    tableOut[i].forEach(function(v,j){if(v==1){tableOut[i][j]='v'+(j+1)}});
    tableOut[i]=tableOut[i].filter (function(v){{return v!=0}});
    arr.push('{'+  tableOut[i].join()+'}');           
  }
  var str=arr.join();
  writeS('задача 7',[['минимальные внешне устойчивые множества\n'+str]],23,7);
}
