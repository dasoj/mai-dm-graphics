# mai-DM-Graphics



## Название
Программная визуализация отображения графических материалов

## Описание
Проект представляет собой набор решений для автоматизации решения задач по дискретной математике в среде Google Sheets с использованием Google Apps Script.

Эти инструменты позволяют пользователям не только эффективно решать задачи, связанные с бинарными отношениями, матрицами смежности и другими элементами дискретной математики, но и визуализировать результаты в удобной и понятной форме, делая процесс обучения и анализа более доступным и эффективным.

## Использование
https://docs.google.com/spreadsheets/d/1U-BmgUi-T1PrddRhkAWtlw-ScJ5bmHaEmy8VEW0KHUk/edit?usp=sharing

https://docs.google.com/spreadsheets/d/1L2y5cgmoNs119seQmwDJ8ajofKBVnT6THgMD7_6snus/edit?usp=sharing

https://docs.google.com/spreadsheets/d/1JCJFe4hAJBWizpWWOtWQ6gMBdp8FFw6UgiIGRQfHwbg/edit?usp=sharing

Для использования данных примеров, требуется создать копию файлов на свой диск

## Авторы
Голошумов Михаил Сергеевич https://vk.com/goloshumovs

Хрушкова Валерия Витальевна https://vk.com/vkhrushkova

Кон Юлия Вячеславовна https://vk.com/firfirrr

Силаева Владислава Сергеевна https://vk.com/houseofthemouse

Мазурец Кирилл Александрович https://vk.com/kirill_mazurets
